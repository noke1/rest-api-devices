## Run via terminal with building artifacts first

1. Clone repository via `git clone`
2. Open project in IntelliJ
3. Go to `File` > `Project Structure...` > `Artifacts`
4. Click a `plus sign` in the top left corner inside the right pane
5. Select `JAR` > `From modules with dependencies...`
6. Inside field called `Main Class` select a `Main.java` class
7. Select checkbox `extract to the target JAR`
8. Change `Directory for META-INF/MAINFEST.MF` from what is written by default to `<project_path>\src\main\resources`
9. Then right-click on the `personal_data_gernerator` folder (inside a tree-view on the right pane) and choose `Put into output root`
10. `Apply` and `OK`
11. Go into `Build` > `Build artifacts`
12. If artifacts were build correctly go into main project folder and run application by `java -jar out/artifacts/personal_data_generator_jar/personal_data_generator.jar`

## Or build artifacts from command line
`mvn clean package` - this should create .apk file inside `target/` directory or override existing one

## Build image from docker-compose
`docker-compose build` - this will build app image

## If trying to start on linux disable mysql
`sudo service mysql stop`

## Run docker-compose
`docker-compose up -d` - this will download database image and run two containers: database container and app container
(Rest api is running on port 7713. To change it browse `application.properties` and rebuild artifacts)

## Browse swagger ui for endpoints info:
Go to SwaggerUI page after successfully running a container:
`http://localhost:7713/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config`
         
