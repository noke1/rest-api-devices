package com.example.rest_app.controller;

import com.example.rest_app.pojo.device.Device;
import com.example.rest_app.pojo.device.DeviceStatus;
import com.example.rest_app.pojo.device.DeviceType;
import com.example.rest_app.repository.DeviceRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class DeviceControllerTest {

    private DeviceRepository deviceRepository = Mockito.mock(DeviceRepository.class);
    private DeviceController deviceController;

    @BeforeEach
    public void beforeEach() {
        deviceController = new DeviceController(deviceRepository);
    }

    @Test
    public void shouldCreateNewDeviceWithSpecifiedCredentials() {

        Device device = new Device(
                (long) 900111,
                DeviceType.XSD,
                "+48 888 555 222",
                DeviceStatus.AVAILABLE
        );
        when(deviceRepository.save(any(Device.class))).then(returnsFirstArg());

        Device newDevice = deviceController.createNewDevice(device);

        assertThat(newDevice).isEqualTo(device);
    }

    @Test
    public void shouldReturnNonEmptyListOfDevices(){
        List<Device> devices = Arrays.asList(new Device(
                (long)900111,
                DeviceType.XSD,
                "+48 888 888 888",
                DeviceStatus.AVAILABLE
        ));
        when(deviceRepository.findAll()).thenReturn(devices);

        List<Device> allDevices = deviceController.getAllDevices();

        assertThat(allDevices).isNotEmpty();
    }

}