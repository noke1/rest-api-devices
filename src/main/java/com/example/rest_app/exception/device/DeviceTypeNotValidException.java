package com.example.rest_app.exception.device;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class DeviceTypeNotValidException extends RuntimeException {


    public DeviceTypeNotValidException() {
        super(String.format("Please provide device type in number format ex. 0,1,2..."));
    }
}
