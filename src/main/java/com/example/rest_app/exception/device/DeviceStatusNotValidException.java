package com.example.rest_app.exception.device;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class DeviceStatusNotValidException extends RuntimeException {


    public DeviceStatusNotValidException() {
        super(String.format("Please provide device status with number format, example: 0,1,2"));
    }
}
