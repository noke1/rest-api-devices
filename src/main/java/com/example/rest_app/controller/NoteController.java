package com.example.rest_app.controller;

import com.example.rest_app.exception.ResourceNotFoundException;
import com.example.rest_app.pojo.note.Note;
import com.example.rest_app.repository.NoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class NoteController {

    private final NoteRepository noteRepository;

    @Autowired
    public NoteController(NoteRepository noteRepository) {
        this.noteRepository = noteRepository;
    }

    @GetMapping("/api/note")
    public List<Note> getAllNotes() {
        return noteRepository.findAll();
    }

    @PostMapping("/api/note")
    public Note createNewNote(@Validated @RequestBody Note note) {
        return noteRepository.save(note);
    }

    @GetMapping("/api/note/{noteId}")
    public Note getNoteById(@PathVariable(value = "noteId") Long noteId) {
        return findNote(noteId);
    }

    @PutMapping("/api/note/{noteId}")
    public Note updateNoteDetails(@PathVariable (value = "noteId") Long noteId,
                                  @Validated @RequestBody Note note) {
        final Note noteToChange = findNote(noteId);

        noteToChange.setDeviceSerialNumber(note.getDeviceSerialNumber());
        noteToChange.setNote(note.getNote());

        return noteRepository.save(noteToChange);
    }

    @DeleteMapping("/api/note/{noteId}")
    public ResponseEntity<?> deleteNote(@PathVariable(value = "noteId") Long noteId) {
        final Note noteToDelete = findNote(noteId);
        noteRepository.delete(noteToDelete);
        return ResponseEntity.ok().build();
    }

    private Note findNote(@PathVariable("noteId") Long noteId) {
        return noteRepository.findById(noteId).orElseThrow(() -> new ResourceNotFoundException(noteId));

    }
}
