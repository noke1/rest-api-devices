package com.example.rest_app.controller;

import com.example.rest_app.exception.ResourceNotFoundException;
import com.example.rest_app.exception.device.DeviceStatusNotValidException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import com.example.rest_app.pojo.device.Device;
import com.example.rest_app.repository.DeviceRepository;

import java.util.List;

@RestController
public class DeviceController {


    private final DeviceRepository deviceRepository;

    @Autowired
    public DeviceController(DeviceRepository deviceRepository) {
        this.deviceRepository = deviceRepository;
    }

    @GetMapping("/api/device")
    public List<Device> getAllDevices(){
        return deviceRepository.findAll();
    }

    @PostMapping("/api/device")
    public Device createNewDevice(@Validated @RequestBody Device device) throws DeviceStatusNotValidException {
        return deviceRepository.save(device);
    }

    @GetMapping("/api/device/{deviceId}")
    public Device getDeviceById(@PathVariable(value = "deviceId") Long deviceId) throws ResourceNotFoundException {
        return findDevice(deviceId);
    }

    @PutMapping("/api/device/{deviceId}")
    public Device updateDeviceDetails(@PathVariable(value = "deviceId") Long deviceId,
                                      @Validated @RequestBody Device deviceDetails) throws ResourceNotFoundException {
        Device deviceToChange = findDevice(deviceId);

        deviceToChange.setDeviceStatus(deviceDetails.getDeviceStatus());
        deviceToChange.setDeviceType(deviceDetails.getDeviceType());
        deviceToChange.setPhoneNumber(deviceDetails.getPhoneNumber());

        return deviceRepository.save(deviceToChange);
    }

    @DeleteMapping("/api/device/{deviceId}")
    public ResponseEntity<?> deleteDevice(@PathVariable(value = "deviceId") Long deviceId) throws ResourceNotFoundException {
        Device deviceToDelete = findDevice(deviceId);

        deviceRepository.delete(deviceToDelete);

        return ResponseEntity.ok().build();
    }



    private Device findDevice(@PathVariable("deviceId") Long deviceId) throws ResourceNotFoundException {
        return deviceRepository.findById(deviceId)
                .orElseThrow(() -> new ResourceNotFoundException(deviceId));
    }


}
