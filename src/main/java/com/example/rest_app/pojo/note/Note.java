package com.example.rest_app.pojo.note;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Note {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long noteId;

    @Column(name = "device_id")
    private Long deviceSerialNumber;

    @NotEmpty
    @NotNull
    @Size(min = 1, max = 200, message = "Please enter between 1 and 200 characters")
    private String note;

    public Note() {
    }

    public Note(Long noteId, Long deviceSerialNumber, String note) {
        this.noteId = noteId;
        this.deviceSerialNumber = deviceSerialNumber;
        this.note = note;
    }

    public Long getNoteId() {
        return noteId;
    }

    public void setNoteId(Long noteId) {
        this.noteId = noteId;
    }

    public Long getDeviceSerialNumber() {
        return deviceSerialNumber;
    }

    public void setDeviceSerialNumber(Long deviceSerialNumber) {
        this.deviceSerialNumber = deviceSerialNumber;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public String toString() {
        return "Note{" +
                "noteId=" + noteId +
                ", deviceSerialNumber=" + deviceSerialNumber +
                ", noteContent='" + note + '\'' +
                '}';
    }
}
