package com.example.rest_app.pojo.device;

import java.util.Arrays;

public enum DeviceType {

    XSD(0),
    GPS(1),
    RFI(2),
    HMI(3),
    MOB(4);

    private int value;

    DeviceType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static DeviceStatus translateValueToStatus(int id){
        return Arrays.stream(DeviceStatus.values())
                .filter(status -> status.getValue() == id)
                .findFirst()
                .orElse(null);
    }
}
