package com.example.rest_app.pojo.device;

import java.util.Arrays;

public enum DeviceStatus {
    AVAILABLE(0),
    BORROWED(1),
    DESTROYED(2);

    private int value;

    DeviceStatus(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static DeviceStatus translateValueToStatus(int id){
        return Arrays.stream(DeviceStatus.values())
                .filter(status -> status.getValue() == id)
                .findFirst()
                .orElse(null);
    }
}
