package com.example.rest_app.pojo.device;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.*;

@Entity
public class Device {

    @Id
    @Min(value = 900000)
    private Long deviceSerialNumber;

    private DeviceType deviceType;

    @NotEmpty
    @NotNull
    @Pattern(regexp = "(?<!\\w)(\\(?(\\+|00)?48\\)?)?[ -]?\\d{3}[ -]?\\d{3}[ -]?\\d{3}(?!\\w)", message = "Please enter a 12 characters number starting from +00 lub +48")
    private String phoneNumber;
    private DeviceStatus deviceStatus;

    public Device() {
    }

    public Device(Long deviceSerialNumber, DeviceType deviceType, String phoneNumber, DeviceStatus deviceStatus) {
        this.deviceSerialNumber = deviceSerialNumber;
        this.deviceType = deviceType;
        this.phoneNumber = phoneNumber;
        this.deviceStatus = deviceStatus;
    }

    public Long getDeviceSerialNumber() {
        return deviceSerialNumber;
    }

    public void setDeviceSerialNumber(Long deviceSerialNumber) {
        this.deviceSerialNumber = deviceSerialNumber;
    }

    public DeviceType getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(DeviceType DeviceType) {
        this.deviceType = DeviceType;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public DeviceStatus getDeviceStatus() {
        return deviceStatus;
    }

    public void setDeviceStatus(DeviceStatus deviceStatus) {
        this.deviceStatus = deviceStatus;
    }

    @Override
    public String toString() {
        return "Device{" +
                "deviceSerialNumber=" + deviceSerialNumber +
                ", deviceType=" + deviceType +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", deviceStatus=" + deviceStatus +
                '}';
    }
}