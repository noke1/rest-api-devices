package com.example.rest_app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.rest_app.pojo.device.Device;

@Repository
public interface DeviceRepository extends JpaRepository<Device,Long> {
}
