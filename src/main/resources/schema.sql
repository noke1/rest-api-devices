DROP TABLE IF EXISTS device;
DROP TABLE IF EXISTS type;
DROP TABLE IF EXISTS status;
DROP TABLE IF EXISTS note;

CREATE TABLE type (
    type_id INT PRIMARY KEY,
    type VARCHAR(3) NOT NULL
);

CREATE TABLE status (
    status_id INT PRIMARY KEY,
    status VARCHAR(20) NOT NULL
);

CREATE TABLE device (
    device_serial_number INT PRIMARY KEY,
    device_type INT NOT NULL,
    phone_number VARCHAR(15) NOT NULL,
    device_status INT NOT NULL,
    FOREIGN KEY (device_type) REFERENCES type(type_id),
    FOREIGN KEY (device_status) REFERENCES status(status_id)
);

CREATE TABLE note (
    note_id INT PRIMARY KEY AUTO_INCREMENT,
    device_id INT NOT NULL,
    content VARCHAR(200) NOT NULL,
    FOREIGN KEY (device_id) REFERENCES device(device_serial_number)
);
