INSERT INTO
    type (type_id,type)
VALUES
    (0,'XSD'),
    (1,'GPS'),
    (2,'RFI'),
    (3,'HMI'),
    (4,'MOB');

INSERT INTO
    status (status_id,status)
VALUES
    (0,'AVAILABLE'),
    (1,'BORROWED'),
    (2,'DESTROYED');

--INSERT INTO
--    device (device_serial_number,device_type,phone_number,device_status,more_info)
--VALUES
--    (900700,1,'+48900900900',1,'TEST123'),
--    (12649553,2,'+48 200 300 400',2,'TEST123');
