FROM openjdk:8-alpine
ADD target/rest_app-0.0.1-SNAPSHOT.jar .
EXPOSE 7713
CMD java -jar rest_app-0.0.1-SNAPSHOT.jar
